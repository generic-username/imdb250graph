/*$XV*/
//var xValues = ["Sep. 18", "Sep. 18", "Sep. 18", "Sep. 19"];

var fullData = {
    labels: xValues,
    datasets: []
}
var tvData = {
    labels: xValues,
    datasets: []
}

/*$MDS*/
/*$TDS*/

var fullDataBackup = [...fullData.datasets];
var tvDataBackup = [...tvData.datasets];

    var chartA = new Chart("chartA", {
      type: "line",
      data: fullData,
      options: {
        maintainAspectRatio: false,
        responsive: true,
        legend: {display: false},
        scales: {yAxes: [{
                        display: true,
                        ticks: {
                            reverse: true,
                            start: 1
                        }
                    }]
                }
      }
    });
    var chartB =new Chart("chartB", {
      type: "line",
      data: tvData,
      options: {
        maintainAspectRatio: false,
        responsive: true,
        legend: {display: false},
        scales: {yAxes: [{
                        display: true,
                        ticks: {
                            reverse: true,
                            start: 1
                        }
                    }]
                }
      }
    });

var slider = document.getElementById("myRange");
slider.oninput = function() {
    document.getElementById("chartAContainer").style.height = (this.value*100) + "px";
    chartA.resize();
}
var tvslider = document.getElementById("tvRange");
tvslider.oninput = function() {
    document.getElementById("chartBContainer").style.height = (this.value*100) + "px";
    chartB.resize();
}
// =================== MOVIE SELECt
var movs = document.getElementById("movies")
for (let i = 0; i < fullData.datasets.length; i++) {
    var option = document.createElement("option");
    var mvData = fullData.datasets[i];
    option.innerHTML = "<option value=\""+mvData.label+"\">"+mvData.label+"</option>"
    movs.appendChild(option);
}
var movieSelect = document.getElementById("movies");
movieSelect.oninput = function() {
    fullData.datasets = [...fullDataBackup]
    var selectedData = null;
    for(let i = 0; i < fullData.datasets.length; i++){
        if(fullData.datasets[i].label === this.value){
            selectedData = fullData.datasets[i].data;
            break;
        }
    }
    var maxRank = Math.max(...selectedData) + 2;
    var minRank = Math.min(...selectedData) - 2;
    fullData.datasets.length = 0;
    for(let i = 0; i < fullDataBackup.length; i++){
        var mv = fullDataBackup[i];
        if(mv.data[mv.data.length - 1] >= minRank && mv.data[mv.data.length - 1] <= maxRank){
            fullData.datasets.push(mv);
        }
    }
    // Update slide
    slider.value = 5;
    document.getElementById("chartAContainer").style.height = (5*100) + "px";
    chartA.update();
    chartA.resize();
}
//============= TV SELECt
var tv = document.getElementById("tv")
for (let i = 0; i < tvData.datasets.length; i++) {
    var option = document.createElement("option");
    var mvData = tvData.datasets[i];
    option.innerHTML = "<option value=\""+mvData.label+"\">"+mvData.label+"</option>"
    tv.appendChild(option);
}
var tvSelect = document.getElementById("tv");
tvSelect.oninput = function() {
    tvData.datasets = [...tvDataBackup]
    var selectedData = null;
    for(let i = 0; i < tvData.datasets.length; i++){
        if(tvData.datasets[i].label === this.value){
            selectedData = tvData.datasets[i].data;
            break;
        }
    }
    var maxRank = Math.max(...selectedData) + 2;
    var minRank = Math.min(...selectedData) - 2;
    tvData.datasets.length = 0;
    for(let i = 0; i < tvDataBackup.length; i++){
        var mv = tvDataBackup[i];
        if(mv.data[mv.data.length - 1] >= minRank && mv.data[mv.data.length - 1] <= maxRank){
            tvData.datasets.push(mv);
        }
    }
    // Update slide
    tvslider.value = 5;
    document.getElementById("chartBContainer").style.height = (5*100) + "px";
    chartB.update();
    chartB.resize();
}