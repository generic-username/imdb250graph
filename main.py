import datetime
import os.path
import sys

import requests
from typing import List
import pickle

import templatefiller


class Movie:
    rank: int
    id: int
    title: str
    image: str
    rating: float
    rating_count: int


class MovieSnapshot:
    def __init__(self):
        self.movies: List[Movie] = []
        self.time = None



class MovieSnapshotModel:
    def __init__(self):
        self.snapshots: List[MovieSnapshot] = []

    def save(self, file_name):
        f = open(file_name, "wb")
        data = pickle.dumps(self.snapshots)
        print(data)
        f.write(data)
        f.close()

    def load(self, file_name):
        if os.path.exists(file_name):
            f = open(file_name, "rb")
            rd = f.read()
            print(rd)
            self.snapshots = pickle.loads(rd)

    def new_snapshot(self, url: str):
        x = requests.get(url)
        js = x.json()
        print("RAW JSON: " + str(js))
        items = js["items"]
        snapshot = MovieSnapshot()
        for item in items:
            # item is a dict
            movie = Movie()
            movie.rank = int(item["rank"])
            movie.id = item["id"]
            movie.title = item["fullTitle"] + ' id:' + movie.id
            movie.image = item["image"]
            movie.rating = float(item["imDbRating"])
            movie.rating_count = int(item["imDbRatingCount"])
            snapshot.movies.append(movie)
        snapshot.time = datetime.datetime.today()
        self.snapshots.append(snapshot)

    def dump(self):
        print("Snapshot:")
        for ss in self.snapshots:
            m_list = ""
            for mv in ss.movies:
                m_list += mv.title + ", "
            print(ss.time)
            print(m_list)


update_from_imdb = True
if sys.argv.__len__() > 1 and sys.argv[1] == "-noupdate":
    update_from_imdb = False


movie_model = MovieSnapshotModel()
movie_model.load("data/mvdata3.tx")
if update_from_imdb:
    movie_model.new_snapshot("https://imdb-api.com/en/API/Top250Movies/k_okft9xyf")
movie_model.save("data/mvdata3.tx")
movie_model.dump()


tv_model = MovieSnapshotModel()
tv_model.load("data/tvdata3.tx")
if update_from_imdb:
    tv_model.new_snapshot("https://imdb-api.com/en/API/Top250TVs/k_okft9xyf")
tv_model.save("data/tvdata3.tx")
tv_model.dump()


templatefiller.create(movie_model, tv_model)
