import math
import random
import colorsys


def read_template() -> str:
    f = open("script_template.js", "r")
    return f.read()


def create_dataset(model, js_data_name):
    movie_dict = {}
    snapshots_ran = 0
    for snapshot in model.snapshots:
        snapshots_ran += 1
        for movie in snapshot.movies:
            if movie.title not in movie_dict:
                movie_dict[movie.title] = []
            movie_dict[movie.title].append(movie.rank)
        for rank_list in movie_dict.values():
            if len(rank_list) <= snapshots_ran - 1:
                rank_list.append(251)

    data_set_str: str = ""
    ran = 0
    color_ran = 0
    for title, array in movie_dict.items():
        data_set_str += js_data_name + '.datasets.push({label: "' + title + '", '

        data_format = "data: ["
        for rank in array:
            data_format += str(rank) + ","
        data_format = data_format[:-1]
        data_format += "],"
        data_set_str += data_format
        ran += 1
        color_ran += 0.05
        color = colorsys.hsv_to_rgb(color_ran % 1, 1, 1)
        color_str = str(color[0] * 255) + ',' + str(color[1] * 255) + ',' + str(color[2] * 255)
        data_set_str += 'borderColor: "rgb(' + color_str + ')",borderWidth: 2,fill: false,tension:0});\n'
    return data_set_str


def create(movie_model, tv_model):
    template_str: str = read_template()

    x_val: str = "var xValues = [\""
    for snapshot in movie_model.snapshots:
        x_val += snapshot.time.strftime("%b. %d")
        x_val += "\", \""
    x_val = x_val[:-3]
    x_val += "];"

    movie_str = create_dataset(movie_model, 'fullData')
    tv_str = create_dataset(tv_model, 'tvData')

    template_str = template_str.replace("/*$XV*/", x_val).replace("/*$MDS*/", movie_str).replace("/*$TDS*/", tv_str)
    f = open("script.js", "w")
    f.write(template_str)
    f.close()
